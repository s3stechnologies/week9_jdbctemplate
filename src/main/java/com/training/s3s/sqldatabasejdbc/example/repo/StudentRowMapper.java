package com.training.s3s.sqldatabasejdbc.example.repo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.training.s3s.sqldatabasejdbc.example.model.Student;

public final class StudentRowMapper  implements RowMapper<Student>{

	
	
	@Override
	public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Student student = new Student();
		student.setId(rs.getInt("id"));
		student.setName(rs.getString("name"));
		student.setAge(rs.getInt("age"));
		
	
		return student;
	}

}
