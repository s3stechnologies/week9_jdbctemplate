package com.training.s3s.sqldatabasejdbc.example.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.s3s.sqldatabasejdbc.example.model.Student;
import com.training.s3s.sqldatabasejdbc.example.service.StudentService;


@RestController
public class StudentController {

	@Autowired
	private StudentService service;

	
	@RequestMapping("/getNameOfStudent/{id}")
	public String getAllStudents(@PathVariable("id") int id) throws Exception {
		
		return service.getNameById(id);
	
	}
	
	
	@PostMapping(value = "/student/poststudent") 
	public String postStudent(@RequestBody Student student) throws Exception {
	  
	  //service.save(student);
		
		service.insertStudent(student);
	  
	  return "Student got added!";
	  
	  }
	
	
	@GetMapping("/student/{id}") 
	public Student  getStudentById(@PathVariable Integer id) {
		
		return service.getStudentByid(id);
	  
	  }
	
	
	
	/*
	 * // create new entry, or data or rows
	 * 
	 * @PostMapping(value = "/student/poststudent") public String
	 * postStudent(@RequestBody List<Student> student) throws Exception {
	 * 
	 * service.postStudent(student);
	 * 
	 * return "Student got added!";
	 * 
	 * }
	 * 
	 * 
	 * // get info or get data or dispaly data
	 * 
	 * @GetMapping("/student/{id}") public Optional<Student>
	 * getStudentById(@PathVariable int id) { return service.getStudentByid(id);
	 * 
	 * }
	 * 
	 * 
	 * @GetMapping("/student/{id}/{name}") public Student
	 * getStudentById(@PathVariable("id") int id, @PathVariable("name") String name)
	 * { return service.getStudentByIdAndName(id, name);
	 * 
	 * }
	 * 
	 * 
	 * @PutMapping("/student/{id}") public String updateStudent(@RequestBody Student
	 * student) {
	 * 
	 * service.updateStudent(student);
	 * 
	 * return "Student Updated";
	 * 
	 * }
	 * 
	 * @DeleteMapping("/student/{id}") public String deleteStudentById(@PathVariable
	 * int id) {
	 * 
	 * service.deleteStudentByid(id);
	 * 
	 * return "Student with " + id + " deleted!";
	 * 
	 * }
	 */
}
