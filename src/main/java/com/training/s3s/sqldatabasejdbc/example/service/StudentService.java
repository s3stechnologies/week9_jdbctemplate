package com.training.s3s.sqldatabasejdbc.example.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;

import com.training.s3s.sqldatabasejdbc.example.model.Student;
import com.training.s3s.sqldatabasejdbc.example.repo.StudentRepo;
import com.training.s3s.sqldatabasejdbc.example.repo.StudentRowMapper;

@Service
public class StudentService implements StudentRepo {
	
	
	Logger logger = LoggerFactory.getLogger(StudentService.class);
	
	@Autowired
	private JdbcTemplate template;
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameter;

	
	
	@Override
	public int save(Student student) throws Exception {
		
		int num = 0;
		logger.info("Inside insert()");
		
		String sql = "INSERT INTO student_table values(?,?,?)";
		
		//String query= "insert into student_table values('"+student.getId()+"','"+student.getName()+"','"+student.getAge()+"')";  
		
		try{
			
			Object[] objectArray = {student.getId(),student.getName(),student.getAge()};
			
			num = template.update(sql, objectArray );
			
		//	logger.info("Student got added");
			
			
		}catch(Exception e) {
			logger.error("Error occured while adding student to the database!");
			throw new Exception();
			
		}
		
		return num;
		
		
	}

	@Override
	public String getNameById(int id) throws Exception {
		
		String name = null;
		
		String sql = "SELECT name FROM student_table WHERE id = ?";
	
		try {
			
		name = template.queryForObject(sql, new Object[] {id},String.class);
			
		}catch(Exception e) {
			throw new Exception();
		}
		
		return name;
	}

	
	public void insertStudent(Student student) {
		
		String sql = "insert into student_table(id,age,name) values(:id; :name ; :age)";
		
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("id", student.getId())
				.addValue("name", student.getName())
				.addValue("age", student.getAge());
		
		namedParameter.update(sql, parameters);
	}

	
	
	public Student getStudentByid(int id) {
		
		String sql = "SELECT * FROM student_table WHERE id = ?";
		
//		SqlParameterSource parameters = new MapSqlParameterSource()
//				.addValue("id", id);
	
		return template.queryForObject(sql, new Object[] {id}, new StudentRowMapper());
	}
	
	
	
	

	
	
}
