package com.training.s3s.sqldatabasejdbc.example.repo;

import com.training.s3s.sqldatabasejdbc.example.model.Student;

public interface StudentRepo {
	

	public int save(Student student)throws Exception ;
	
	public String getNameById(int id) throws Exception;
	
	
}
